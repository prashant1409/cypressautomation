var data ={};
var selectors ={};
describe('Verify INVOICE', () => {
  beforeEach(function () {
    cy.fixture('example').then((data) => {
        this.data=data;
    })
    cy.fixture('webElementSelectors').then((selectors) => {
      this.selectors=selectors;
  })
  })

 
  it('Navigate to phptravels.com/demo and get Homepage front-end url ', function () {
    cy.visit(this.data.url);

    cy.get(this.selectors.homePageFrontEndLink).invoke('attr','href').then(nextLink=>{
      console.log(nextLink);
      cy.writeFile('cypress/fixtures/tempdata.json', { link: nextLink })
    });

    //Verifying title APPLICAITON TEST DRIVE
    cy.get(this.selectors.phpTravelsDemoTitleLabel)
    .should('be.visible')
    .should('have.class','wow fadeIn cw upper animated')
    .should('have.text','Application Test Drive');    
    })

  it('Navigate to phptravels.net, login and verify Invoice', function () {
    cy.fixture('tempdata').then((data) => {
      console.log(data.link);
      cy.visit("https:" + data.link);
  })

    cy.get(this.selectors.myAccountDropdownToggle).click();
    cy.get(this.selectors.myAccountLogin).click();

    cy.get(this.selectors.userName).type(this.data.username);
    cy.get(this.selectors.password).type(this.data.password);
    cy.get(this.selectors.loginButton).click();
    
    cy.get(this.selectors.invoiceLink).invoke('attr','href').then(nextLink=>{
      console.log(nextLink);
      cy.visit(nextLink);

      cy.contains("Invoice Number").parent().prev()
      .should('have.text','Invoice')
      .should('have.attr','style','font-size: 34px;text-transform:uppercase;font-weight: bold;');

    });
  })

})
